
var cling = keyboard_check_pressed(vk_space) || gamepad_button_check_pressed(0, gp_face3);
var cling_held = keyboard_check(vk_space) || gamepad_button_check(0, gp_face3);

// timers
cling_delay_timer = cling_delay_timer - 1;

if(cling && inAir){
    var cling_check_distance = direction_facing * ((sprite_width/2) + 2);
    
    collision_front_stationary = place_meeting(x + hs + cling_check_distance, y, pHardBlock);
    
    if(collision_front_stationary && cling_delay_timer <= 0){
        vs = 1;
        is_clung = true;
    }else{
        is_clung = false;
    }
}

// energy depletion
if(is_clung){
    if(cling_time > 0){
        cling_time--;
    }
} else if(!inAir){
    if(cling_time < cling_time_const){
        cling_time+=2;
    }else{
        cling_time = cling_time_const;
    }
}

var cling_check_distance = direction_facing * ((sprite_width/2) + 2);

collision_front_stationary = place_meeting(x + hs + cling_check_distance, y, pHardBlock);

if(!collision_front_stationary || !cling_held){
    is_clung = false;
}
if(cling_time <=1){
    is_clung = false;
    cling_delay_timer = 10;
}

